from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import redirect
from .models import UserDetails, UserActivityDetails
# Create your views here.
import requests
import datetime


def homepage(request):
    user_select_type = ["education", "recreational", "social",
                        "diy", "charity", "cooking", "relaxation", "music", "busywork"]
    user = request.user
    print(str(user.username), "dskj")
    user_all_activity = ""


    if user.username != "":
        user_details = UserDetails.objects.get(user=user)
        if request.method == "POST" and request.POST.get('one_more_activity') == "one_more_activity":
            print(user_details.last_updated,datetime.datetime.now(),user_details.cout_per_day)
            today_date = datetime.datetime.now()
            today_date = today_date.date()
            user_details_last_updated = user_details.last_updated
            user_details_last_updated = user_details_last_updated.date()
            print(user_details_last_updated,today_date)
            if user_details_last_updated == today_date and user_details.cout_per_day == 2:
                user_all_activity = UserActivityDetails.objects.filter(user_details=user_details)
                warning = "{} can't add other activity today".format(request.user.username)
                print(warning)
                context = {
                "user_select_type": user_select_type,
                "user_all_activity": user_all_activity,
                "user_details":user_details,
                "warning":warning
                }
                return render(request, "activity/homepage.html", context)    
            elif user_details_last_updated == today_date  and user_details.cout_per_day < 2:
                user_details.cout_per_day = user_details.cout_per_day + 1
                user_details.save()
                add_one_more_activity = requests.get("https://www.boredapi.com/api/activity?type=".format(user_details.activity_type)).json()
                add_one_more_activity_save = UserActivityDetails(
                    user_details=user_details,
                    activity=add_one_more_activity["activity"],
                    activity_type=add_one_more_activity["type"]
                    )
                add_one_more_activity_save.participants = add_one_more_activity["participants"]
                add_one_more_activity_save.link = add_one_more_activity["link"]
                add_one_more_activity_save.accessibility = add_one_more_activity["accessibility"]
                # add_one_more_activity_save.count_activity = cout_i + 1
                add_one_more_activity_save.save()

                
            elif user_details_last_updated != today_date  and user_details.cout_per_day < 2:
                user_details.cout_per_day = user_details.cout_per_day + 1
                user_details.last_updated = datetime.datetime.now()
                user_details.save()
                add_one_more_activity = requests.get("https://www.boredapi.com/api/activity?type=".format(user_details.activity_type)).json()
                add_one_more_activity_save = UserActivityDetails(
                    user_details=user_details,
                    activity=add_one_more_activity["activity"],
                    activity_type=add_one_more_activity["type"]
                    )
                add_one_more_activity_save.participants = add_one_more_activity["participants"]
                add_one_more_activity_save.link = add_one_more_activity["link"]
                add_one_more_activity_save.accessibility = add_one_more_activity["accessibility"]
                # add_one_more_activity_save.count_activity = cout_i + 1
                add_one_more_activity_save.save()

            elif user_details_last_updated != today_date  and user_details.cout_per_day == 2:
                user_details.cout_per_day = 1
                user_details.last_updated = datetime.datetime.now()
                user_details.save()
                add_one_more_activity = requests.get("https://www.boredapi.com/api/activity?type=".format(user_details.activity_type)).json()
                add_one_more_activity_save = UserActivityDetails(
                    user_details=user_details,
                    activity=add_one_more_activity["activity"],
                    activity_type=add_one_more_activity["type"]
                    )
                add_one_more_activity_save.participants = add_one_more_activity["participants"]
                add_one_more_activity_save.link = add_one_more_activity["link"]
                add_one_more_activity_save.accessibility = add_one_more_activity["accessibility"]
                # add_one_more_activity_save.count_activity = cout_i + 1
                add_one_more_activity_save.save()    


            user_all_activity = UserActivityDetails.objects.filter(user_details=user_details)
            context = {
                "user_select_type": user_select_type,
                "user_details":user_details,
                "user_all_activity": user_all_activity,
             }
            return render(request, "activity/homepage.html", context)    
                 
        len_of_activity_of_user = UserActivityDetails.objects.filter(user_details=user_details)
        if len(len_of_activity_of_user) > 3:
            print(len_of_activity_of_user)
        else:
            for cout_i in range(0, 10):
                print(cout_i)
                activity = requests.get("https://www.boredapi.com/api/activity?type={}".format(user_details.activity_type)).json()
                # activity = (activity.json())
                print(activity)
                try:
                    user_activitydetails = UserActivityDetails.objects.get(
                    user_details=user_details,
                    activity=activity["activity"],
                    activity_type=activity["type"]
                    )
                    user_activitydetails.participants = activity["participants"]
                    user_activitydetails.link = activity["link"]
                    user_activitydetails.accessibility = activity["accessibility"]
                    user_activitydetails.save()
                except UserActivityDetails.DoesNotExist:
                    user_activitydetails = UserActivityDetails(
                    user_details=user_details,
                    activity=activity["activity"],
                    activity_type=activity["type"]
                    )
                    user_activitydetails.participants = activity["participants"]
                    user_activitydetails.link = activity["link"]
                    user_activitydetails.accessibility = activity["accessibility"]
                    user_activitydetails.count_activity = cout_i + 1
                    user_activitydetails.save()

        #
        user_all_activity = UserActivityDetails.objects.filter(
            user_details=user_details)
        context = {
        "user_select_type": user_select_type,
        "user_all_activity": user_all_activity,
        "user_details":user_details,
        }
        return render(request, "activity/homepage.html", context)    

    # if the form method is post
    # get the value of the email and password
    # to sign up the new users
    if request.method == "POST":
        user_email = request.POST.get('email')
        user_password = request.POST.get('password')
        type_of_activity = request.POST.get('type')
        admin = request.POST.get('admin')
        print(type_of_activity)

        try:
            # in  signup form try to sign in existing account it will show account alreadyexists
            get_user_details = User.objects.get(username=user_email)
            print(get_user_details)
            context = {
                "warning": 'Email-Id "%s" is already in use.' % user_email,
                "user_select_type": user_select_type,
                "user_details":user_details,

            }
            return render(request, "activity/homepage.html", context)
        # if doesn't exists
        except User.DoesNotExist:
            # user name and password stored to the User model
            user_object = User.objects.create_user(
                username=user_email, email=user_email, 
                password=user_password,
                 )
            user_object.save()
            print(user_object)
            username = user_email
            password = user_password

            # its checking the user name and password it aldready exist
            authenticate_user = authenticate(
                username=user_email, password=user_password)
            print("user", authenticate_user)
            #  it will be login to that user
            if authenticate_user:
                login(request, authenticate_user)
                print(type_of_activity)
                user_detail = UserDetails(
                    user=user_object,
                     activity_type=type_of_activity,
                    admin=admin,
                )
                user_detail.save()

                return redirect("activity:homepage")
        context = {
            "user_select_type": user_select_type,
            "user_details":user_details,
        }
        return render(request, 'activity/homepage.html', context)

    context = {
        "user_select_type": user_select_type,
        "user_all_activity": user_all_activity,
    }
    return render(request, "activity/homepage.html", context)


def activity_page(request, num=0):
    activity_details = UserActivityDetails.objects.get(pk=num)
    context = {
        "activity_details": activity_details
    }
    return render(request, 'activity/activity_page.html', context)

def delete_activity_page(request,num=0):
    activity_details = UserActivityDetails.objects.get(pk=num)
    activity_details.delete()
    return redirect("activity:homepage")

def edit_activity_page(request ,num=0):
    activity_details = UserActivityDetails.objects.get(pk=num)
    if request.method == "POST":
        activity = request.POST.get('activity')
        activity_type = request.POST.get('type')
        link = request.POST.get('link')
        accessibility = request.POST.get('accessibility')
        activity_details.activity = activity
        activity_details.activity_type = activity_type
        activity_details.link = link
        activity_details.accessibility = accessibility

        activity_details.save()
        return redirect("activity:activity_page" ,num=activity_details.pk)

    context = {
        "activity_details": activity_details
    }
    return render(request, 'activity/edit_activity_page.html', context)

def login_page(request):
    message = ""
    # if user enter the correct email and password.
    # it will  login the user and redirect to the homepage
    if request.method == 'POST':
        username = request.POST['email']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            login(request, user)
            return redirect("activity:homepage")
        else:
            message = "Incorrect username / password "

    print("message", message)
    context = {"message": message}
    return render(request, 'activity/login_page.html', context)

#not used


def signup_page(request):
    user_select_type = ["education", "recreational", "social",
                        "diy", "charity", "cooking", "relaxation", "music", "busywork"]
    context = {
        "user_select_type": user_select_type,
    }
    return render(request, "activity/signup_page.html", context)


@login_required
def logout_page(request):
    print(request)
    logout(request)
    print(logout)
    return redirect("activity:homepage")
