from django.contrib import admin
from .models import UserDetails,UserActivityDetails
# Register your models here.

admin.site.register(UserDetails)
admin.site.register(UserActivityDetails)