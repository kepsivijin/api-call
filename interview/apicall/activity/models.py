from django.db import models
import datetime
from django.contrib.auth.models import User

class UserDetails(models.Model):
    user = models.ForeignKey(User,on_delete=models.PROTECT)
    activity_type = models.CharField(blank=True,max_length=50)
    admin = models.CharField(default="no",max_length=50)
    last_updated =  models.DateTimeField(default=datetime.datetime.now, blank=True)
    cout_per_day = models.IntegerField(default=0)


class UserActivityDetails(models.Model):
    user_details = models.ForeignKey(UserDetails,on_delete=models.PROTECT)
    activity_type = models.CharField(blank=True,max_length=50)
    activity = models.CharField(blank=True,max_length=200)
    participants = models.CharField(blank=True,max_length=50)
    link = models.CharField(blank=True,max_length=50)
    accessibility = models.CharField(blank=True,max_length=50)
    count_activity = models.IntegerField(null=True,blank=True)