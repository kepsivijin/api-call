from django.conf.urls import url
from django.contrib import admin
from activity import views
from django.urls import path, re_path

app_name = "activity"

urlpatterns = [
    path('', views.homepage, name="homepage"),
    path('login_page',
         views.login_page, name="login_page"),
    path('signup_page',
         views.signup_page, name="signup_page"),     
    path('logout_page',
         views.logout_page, name="logout_page"),
    path('activity_page/<int:num>',
         views.activity_page, name="activity_page"),
     path('edit_activity_page/<int:num>',
         views.edit_activity_page, name="edit_activity_page"),      
     path('delete_activity_page/<int:num>',
         views.delete_activity_page, name="delete_activity_page"),      
     

]